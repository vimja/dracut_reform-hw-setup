#!/bin/bash

check() {

    MACHINE=$(tr -d '\0' < /proc/device-tree/model)

    if [[ $MACHINE != 'MNT Reform 2'* ]]; then
        return 1;
    fi

    if [ "MNT Reform 2" = "$MACHINE" ] || [ "MNT Reform 2 HDMI" = "$MACHINE" ]; then
        require_any_binary amixer || return 1
        require_kernel_modules snd_soc_wm8960 || return 1
    fi

    if [ "MNT Reform 2 with BPI-CM4 Module" = "$MACHINE" ]; then
        require_any_binary amixer || return 1
        require_kernel_modules snd_soc_wm8960 mdio_mux_meson_g12a dwmac_meson8b || return 1
    fi

    if [ "MNT Reform 2 with LS1028A Module" = "$MACHINE" ]; then
        require_any_binary gpioset || return 1
        require_kernel_modules dwc3 || return 1
    fi

    if [ "MNT Pocket Reform with i.MX8MP Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Pocket Reform with BPI-CM4 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform 2 with i.MX8MP Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform 2 with RCORE RK3588 Module" = "$MACHINE" ]; then
        require_any_binary gpioset || return 1
        require_any_binary amixer || return 1
        require_kernel_modules dwmac_rk snd_soc_wm8960 snd_soc_audio_graph_card snd_soc_rockchip_i2s_tdm snd_soc_simple_card_utils snd_soc_core snd_compress ac97_bus snd_pcm_dmaengine snd_pcm snd_timer snd soundcore || return 1
    fi

    if [ "MNT Pocket Reform with RCORE RK3588 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform Next with RCORE RK3588 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    return 0;
}

install() {
    MACHINE=$(tr -d '\0' < /proc/device-tree/model)

    if [ "MNT Reform 2" = "$MACHINE" ] || [ "MNT Reform 2 HDMI" = "$MACHINE" ]; then
        inst_multiple amixer
    fi

    if [ "MNT Reform 2 with BPI-CM4 Module" = "$MACHINE" ]; then
        inst_multiple amixer
    fi

    if [ "MNT Reform 2 with LS1028A Module" = "$MACHINE" ]; then
        inst_multiple gpioset
    fi

    if [ "MNT Pocket Reform with i.MX8MP Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Pocket Reform with BPI-CM4 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform 2 with i.MX8MP Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform 2 with RCORE RK3588 Module" = "$MACHINE" ]; then
        inst_multiple gpioset amixer
    fi

    if [ "MNT Pocket Reform with RCORE RK3588 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform Next with RCORE RK3588 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    # Add the hook script. It will add the reform-hw-setup script to initque.
    inst_hook pre-trigger 99 "$moddir/reform-hw-setup-hook.sh"
    # Our script needs to run before the cryptsetup password prompt. This is is
    # so the keyboard will be working oin LS1028A for sure. Scripts in
    # initqueue are run in alphabetical order. So by adding the 001 prefix, we
    # ensure early execution.
    inst_simple "$moddir/001_reform-hw-setup.sh" /sbin/001_reform-hw-setup.sh
}

installkernel() {
    MACHINE=$(tr -d '\0' < /proc/device-tree/model)

    if [ "MNT Reform 2" = "$MACHINE" ] || [ "MNT Reform 2 HDMI" = "$MACHINE" ]; then
        instmods snd_soc_wm8960
    fi

    if [ "MNT Reform 2 with BPI-CM4 Module" = "$MACHINE" ]; then
        instmods snd_soc_wm8960 mdio_mux_meson_g12a dwmac_meson8b
    fi

    if [ "MNT Reform 2 with LS1028A Module" = "$MACHINE" ]; then
        instmods dwc3
    fi

    if [ "MNT Pocket Reform with i.MX8MP Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Pocket Reform with BPI-CM4 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform 2 with i.MX8MP Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform 2 with RCORE RK3588 Module" = "$MACHINE" ]; then
        instmods dwmac_rk snd_soc_wm8960 snd_soc_audio_graph_card snd_soc_rockchip_i2s_tdm snd_soc_simple_card_utils snd_soc_core snd_compress ac97_bus snd_pcm_dmaengine snd_pcm snd_timer snd soundcore || return 1
    fi

    if [ "MNT Pocket Reform with RCORE RK3588 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi

    if [ "MNT Reform Next with RCORE RK3588 Module" = "$MACHINE" ]; then
        # TODO
        return 1
    fi
}
