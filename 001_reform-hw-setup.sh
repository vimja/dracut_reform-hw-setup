#!/bin/sh

set -u

case "$(cat /proc/device-tree/model)" in
"MNT Reform 2"|"MNT Reform 2 HDMI")
    # Workaround for WM8960 sometimes not coming out of reset
    echo 2-001a > /sys/bus/i2c/drivers/wm8960/bind

    # This switch in WM8960 needs to be on for the headset mic input to work
    amixer -c 0 sset 'Left Input Mixer Boost' on

    # Fix WM8960 being too quiet
    amixer -c 0 sset Playback 255

    # Enable wakeup from suspend on all UARTs (i.MX8MQ)
    echo enabled > /sys/devices/platform/soc@0/30800000.bus/30860000.serial/tty/ttymxc0/power/wakeup
    echo enabled > /sys/devices/platform/soc@0/30800000.bus/30890000.serial/tty/ttymxc1/power/wakeup
    echo enabled > /sys/devices/platform/soc@0/30800000.bus/30880000.serial/tty/ttymxc2/power/wakeup
    ;;
"MNT Reform 2 with BPI-CM4 Module"|"MNT Pocket Reform with BPI-CM4 Module")
    # Workaround for WM8960 sometimes not coming out of reset
    echo 1-001a > /sys/bus/i2c/drivers/wm8960/bind

    # Fix WM8960 being too quiet
    amixer -c 0 sset Playback 255
    amixer -c 0 sset 'Left Output Mixer PCM' on
    amixer -c 0 sset 'Right Output Mixer PCM' on

    # A311D sound controls
    # without setting these, there is no UCM profile and messages like this are produced:
    # no backend DAIs enabled for fe.dai-link-0, possibly missing ALSA mixer-based routing or UCM profile
    amixer -c 0 sset 'FRDDR_A SINK 1 SEL' 'OUT 1'
    amixer -c 0 sset 'FRDDR_A SRC 1 EN' 'on'
    amixer -c 0 sset 'TDMOUT_B SRC SEL' 'IN 0'
    amixer -c 0 sset 'TDMIN_B SRC SEL' 'IN 1'
    amixer -c 0 sset 'TODDR_A SRC SEL' 'IN 1'

    # Workaround for ethernet PHY reset problem

    rmmod dwmac_meson8b dwmac_generic stmmac_platform stmmac mdio_mux_meson_g12a mdio_mux of_mdio
    sleep 0.5
    modprobe mdio_mux_meson_g12a
    modprobe dwmac_meson8b
    ;;
"MNT Reform 2 with LS1028A Module")
    # Workaround for a DWC3 USB Controller regression

    # Assert USB hub reset
    gpioset 2 13=0
    # Reload DWC3 module
    rmmod dwc3
    modprobe dwc3
    # Deassert USB hub reset
    gpioset 2 13=1

    # Select "ondemand" CPU frequency scaling governor
    echo ondemand > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor

    # the functionality of the dwc3 module depends on the loglevel
    # loglevel=3 makes usb not work
    # loglevel=7 makes usb work most of the time
    # https://community.mnt.re/t/no-keyboard-input-with-ls1028-board-and-latest-image/1994/13
    dmesg -n 7
    ;;
"MNT Reform 2 with RCORE RK3588 Module") : ;;
"MNT Pocket Reform with i.MX8MP Module") : ;;
esac
